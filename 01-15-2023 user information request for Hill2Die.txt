-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

On January 15th, 2023, the Hill2Die forums user "speed" created a post which held a minors personal information (dox), as well as information about his school and his family. This problem has been resolved and ended in a supposed police report.

*THIS TRANSPARENCY NOTICE CONTAINS _NO_ PERSONAL INFO!*

Here is a rundown of information collected from the forum user, that was used in the subsequent report:
Username
E-Mail
Registration date
>> the post
Post date
Subject
Body (content)

Since  Hill2Die is hosted on Tor, every IP address resolves as 127.0.0.1, making them irrelevant.

I have shut down the forums as of today and may keep them down until I figure out how to continue. I need to harden the settings as well as the moderation on the forum.

Special thanks to [Jane Doe] for helping me resolve this case.

~ lostbox66 ~
-----BEGIN PGP SIGNATURE-----

iQIzBAEBCAAdFiEEc/WlzMN+irhhP5u7A3M6k48ZNtIFAmPEx1wACgkQA3M6k48Z
NtIjMQ/9FRjeYSqhaD2RI9bDBXaSV0rytRe2Fs/KPjoB+VwLAOsQNv3MY6c9ojaX
WIZ3AZB099r3SaUPSw9f6TPS9nBxdmnAbNI1B92px1Zc4DP3vtAvwLQQWu94iJs0
itk+LJXjgkLfuuuGozy8mnxtuO3zw+j3bkhWBbmQTgxfGIf9Z4TW7YkgM4QzpCMK
g+TGovN62up4BsmRQPVZahh0PlQSR7yxE5Ou7hwz5y2nalZZkGSsNpYFJFGtNd6l
QIfCKO+GM8S5ArhnHSjt4YqPS9DUtAwTV64wVO/UREEGa/tyrt2r7FQCFSe9wbIF
4Ar/pXohngtdBDYfXCx3MTbInuCImNJfvIiSmvz0E3KDb97VEzWvSr4jqXW7/yK4
iTl1Tya+VEicvlMTyF/5QT7kllj21Eg04AEmrTxn020YfEXI8DCpT3+l0uZjk8l2
Bmawz+5qMLZ5XANGoU8goDVH6XiKIgE88oJTMKw8uR4aizwMCrSzdclwUDKXrgCx
wSWuwAVkQN9IWfx7TeNaLfVY2s/bil5DD4eTg6geV0nmasEFWj03T69r6R+cAEXU
bmJml6BjxS6oQona1Y6oRank9WTiZqi92SgssrrGRVaXtgHwDjGbNDMeRzBn2xm/
iS1I41kDSsosN0uE8jnrwV1GZDtGZzXXVPFcYxpz0SACYhmUaDY=
=1zn0
-----END PGP SIGNATURE-----
